#!/usr/bin/env bash

echo "INITIALIZING AIRFLOW USER"

# Ensure that assigned uid has entry in /etc/passwd that is required for Openshift.
# Check whether there is a passwd entry for the container UID
myuid=$(id -u)
mygid=$(id -g)
# turn off -e for getent because it will return error code in anonymous uid case
set +e
uidentry=$(getent passwd $myuid)
set -e

# If there is no passwd entry for the container UID, attempt to create one
if [ -z "$uidentry" ] ; then
    if [ -w /etc/passwd ] ; then
	    echo "$myuid:x:$myuid:$mygid:${USER:-anonymous uid}:$AIRFLOW_HOME:/bin/false" >> /etc/passwd
    else
	    echo "Container ENTRYPOINT failed to add passwd entry for anonymous UID"
	    exit 1
    fi
fi

case "$1" in
  webserver)
    echo "INITIALIZING DATABASE AND WEB ACCESS"
    airflow initdb

    if [[ -n $AIRFLOW__WEBSERVER__RBAC ]]; then
        echo "Configuring web ui access"
        airflow create_user --username $AIRFLOW_USER_ACCOUNT_NAME --password $AIRFLOW_USER_ACCOUNT_PASS --firstname $AIRFLOW_USER_ACCOUNT_NAME --lastname $AIRFLOW_USER_ACCOUNT_NAME --email $AIRFLOW_USER_NOTIFICATION_EMAIL --role Admin
    fi

    if [[ -n $NXCALS_API_KEYTAB && -n $NXCALS_API_PRINCIPAL ]]; then
        echo "Configuring nxcals-api keytab"
        airflow variables --set nxcals-api-keytab-path $NXCALS_API_KEYTAB
        airflow variables --set nxcals-api-principal $NXCALS_API_PRINCIPAL
    fi

    if [[ -n $HADOOP_KEYTAB && -n $HADOOP_PRINCIPAL ]]; then
        echo "Configuring hadoop keytab"
        airflow variables --set hadoop-keytab-path $HADOOP_KEYTAB
        airflow variables --set hadoop-principal $HADOOP_PRINCIPAL
    fi

    airflow connections --add --conn_id "yarn-cluster" --conn_host "yarn" --conn_type yarn --conn_extra '{"queue": "root.default", "deploy-mode": "cluster"}'

    echo "STARTING WEBSERVER"
    exec airflow webserver
    ;;
  scheduler)
    echo "STARTING SCHEDULER"
    exec airflow scheduler
    ;;

  *)
    echo "proceeding in pass-through mode..."
    ;;
esac
