# Spark/NXCALS on Airflow - example configuration

This repository is an example of how to configure and setup airflow spark jobs in cluster mode

# Known production instances

 - [LHC SM Airflow on Openshift](https://gitlab.cern.ch/LHCData/lhc-sm-scheduler)

# Quickstart

### Build latest docker image 

Docker image will contain:
- [latest AirFlow version](Dockerfile) (from tag)
- [entrypoint](entrypoint.sh) that allows to setup the image
- [Spark/NXCALS operator plugin](plugins) that simplifies submission of jobs to nxcals cluster

```bash
docker build -t gitlab-registry.cern.ch/db/spark-service/docker-registry/airflow:v1.10.9 .
```

### Configure your NXCALS Plugin

- mount CMVFS at `/cvmfs` 

    https://cernvm.cern.ch/portal/filesystem/quickstart

- generate NXCALS API and HADOOP keytabs

    ```bash
    cern-get-keytab --login <service-account> --user --password <service-account-pass> --keytab /etc/airflow/nxcals.keytab
    cern-get-keytab --login <service-account> --user --password <service-account-pass> --keytab /etc/airflow/hadoop.keytab
    ```
    
- for a dedicated `docker-compose.yml` file, following ENV variables are supported

    ```
    Entrypoint airflow instance web UI env variables (user, email and password for single-tenant access to Airflow UI):
    - AIRFLOW_USER_ACCOUNT_NAME=<airflow-instance-username>
    - AIRFLOW_USER_NOTIFICATION_EMAIL=<airflow-instance-username>@CERN.CH
    - AIRFLOW_USER_ACCOUNT_PASS=<airflow-instance-pass>
     
    Entrypoint hadoop/nxcals-api keytab env variables:
    - NXCALS_API_KEYTAB=/etc/airflow/nxcals.keytab
    - NXCALS_API_PRINCIPAL=<hadoop-username>@CERN.CH
    - HADOOP_KEYTAB=/etc/airflow/hadoop.keytab
    - HADOOP_PRINCIPAL=<hadoop-username>@CERN.CH
    ```

### Using NXCALS Plugin

```
import os

from airflow.utils.dates import days_ago
from airflow.models import DAG

from airflow.operators.nxcals_plugin import SparkNXCALSOperator

args = {
    'owner': 'airflow',
    'start_date': days_ago(2),
}

dag = DAG(
    dag_id='nxcals_example',
    default_args=args,
    schedule_interval=None,
    tags=['nxcals']
)

dn = os.path.dirname(os.path.realpath(__file__))

nxcals_operator_run = SparkNXCALSOperator(
    application=os.path.join(dn, 'nxcals_example_job.py'),
	task_id='nxcals_query',
    dag=dag,
    conf={},
)
```

### Run docker-compose

Shutdown last instance

```bash
docker-compose down
```

Start instance

```bash
docker-compose up -d
```

Get airflow instance logs
```bash
docker logs airflow -f
```

Login to the instance

![](misc/weblogin.png)

Start your dag

![](misc/webui.png)

Check logs of the dag

![](misc/webtask.png)

