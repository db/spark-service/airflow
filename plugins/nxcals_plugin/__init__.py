from __future__ import division, absolute_import, print_function

from airflow.plugins_manager import AirflowPlugin

from nxcals_plugin.operators.nxcals_operator import SparkNXCALSOperator

class SparkNXCALSPlugin(AirflowPlugin):
    name = "nxcals_plugin"
    operators = [
        SparkNXCALSOperator
    ]